import { createApp } from 'vue'
import App from './App.vue'
import './assets/globel.css'

createApp(App).mount('#app')
